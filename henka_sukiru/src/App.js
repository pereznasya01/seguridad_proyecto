import React from 'react';
import { Routes, Route, useLocation } from 'react-router-dom';
import Layout from "./pages/Layout";
import Contacto from "./pages/Contacto";
import Proyectos from "./pages/Proyecto";
import Home from "./pages/Home";
import Servicios from "./pages/Servicios";

import Editable from './pages/Editable';
import './app.css';
import Galeria from './pages/Galeria';
import Pintura from './pages/Pintura';
import Paileria from './pages/Paileria';
import Automati from './pages/Automati';
import Footer from './pages/Footer';
import Nosotros from './pages/Nosotros';

function App() {
  const location = useLocation();
  const isEditableRoute = location.pathname === '/edicion';

  return (
    <>
      <Routes>
        <Route path='/' element={<Layout />}>
          <Route path='contacto' element={<Contacto />} />
          <Route path='servicio' element={<Servicios />} />
          <Route path='proyecto' element={<Proyectos />} />
          <Route path='nosotros' element={<Nosotros />} />
          <Route path='/' element={<Home />} />
          <Route path='*' element={<Home />} />
          <Route path='paileria' element={<Paileria />} />
          <Route path='pintura-electrostatica' element={<Pintura />}></Route>
          <Route path='automatizacion' element={<Automati />} />
        </Route>
        <Route path='/edicion' element={<Editable />} />
        <Route path='galeria' element={<Galeria />} />
      </Routes>
      {!isEditableRoute && <Footer />}
    </>
  );
}

export default App;

