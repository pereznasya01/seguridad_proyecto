import React, { Fragment, useEffect, useState } from 'react';
import Modal from 'react-modal';

const Edicion4 = () => {
  const [data, setData] = useState([]);
  const [modalOpen, setModalOpen] = useState(false);
  const [id, setId] = useState('');
  const [hora, setHora] = useState('');
  const [editedHo, setHoraEdited] = useState('');

  useEffect(() => {
    fetch('http://localhost:5000/horario')
      .then(response => response.json())
      .then(data => {
        console.log(data);
        setData(data);
      })
      .catch(err => console.log(err));
  }, []);

  const handleLoginClick = horario => {
    setId(horario.id);
    setHora(horario.horario);
    setHoraEdited(horario.horario);
    setModalOpen(true);
  };

  const actualizarInformacion = e => {
    e.preventDefault();

    fetch('http://localhost:5000/update5', {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ id: id, horario: editedHo }),
    })
      .then(response => {
        if (!response.ok) {
          throw new Error('Error de servidor');
        }
        return response.json();
      })
      .then(data => {
        console.log(data);
        setHora(editedHo);
        window.location.reload();
      })
      .catch(err => console.log('Error al actualizar ' + err));
  };

  const handleChange = e => {
    setHoraEdited(e.target.value);
  };

  return (
    <Fragment>
      <h4 id='sub'>Horarios</h4>
      <div>
        <table className='table table-bordered table-responsive'>
          <thead>
            <tr>
              <th>Id</th>
              <th>Horario</th>
              <th>Acción</th>
            </tr>
          </thead>
          <tbody>
            {data.map((horario, index) => {
              return (
                <tr key={index}>
                  <td id='foot1'>{horario.id}</td>
                  <td id='foot1'>{horario.horario}</td>
                  <td>
                    <button id='foot1' className='btn btn-dark p-1 m-1 h8' onClick={() => handleLoginClick(horario)}>
                      Editar
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
      <Modal className='custom-modal' overlayClassName='custom-modal-overlay' isOpen={modalOpen} onRequestClose={() => setModalOpen(false)} appElement={document.getElementById('root')}>
        <div id='inicio_ses'>
          <form className='form' onSubmit={actualizarInformacion}>
            <h2 className='text-left'>Editar información</h2>
            <div className='form-group1'>
              <label id='Id2'>Id:{id}</label>
              <label id='Id2'>Horario</label>
              <input type='text' id='foot1' name='titulo' value={editedHo} onChange={handleChange} />
            </div>
            <br></br>
            <div className='form-group1'>
              <button type='submit'>Editar</button>
            </div>
          </form>
        </div>
      </Modal>
    </Fragment>
  );
};

export default Edicion4;