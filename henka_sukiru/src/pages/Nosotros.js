import React, { useState, useEffect } from 'react';
import './nosotros.css';

function Nosotros() {
  const [data, setData] = useState([]);
  const [data2, setData2] = useState([]);
  const [data3, setData3] = useState([]);

  useEffect(() => {
    fetch('http://localhost:5000/info')
      .then((response) => response.json())
      .then((data) => {
        setData(data[0]);
      })
      .catch((err) => console.log(err));

    fetch('http://localhost:5000/mision')
      .then((response) => response.json())
      .then((data2) => {
        setData2(data2);
      })
      .catch((err) => console.log(err));

    fetch('http://localhost:5000/valores')
      .then((response) => response.json())
      .then((data3) => {
        setData3(data3);
      })
      .catch((err) => console.log(err));
  }, []);

  return (
    <div className="container">
      <div className="row">
        <div className="col-lg-6 col-sm-12">
          <h4 className="text-center" id="subtitulo">
            ¿Quiénes somos?
          </h4>
          <div className="info-form p-2">
            <p id="text1">{data.descripcion}</p>
          </div>
          <div className="col-lg-6 col-sm-12 image-container">
            <img src="images/20200529_172158.jpg" alt="Image 1" className="image-back" style={{ position: "absolute", top: "10px", left: "0px", zIndex: "0", height: "250px", "@media (maxWidth: 768px)": { transform: "translateX(-50%)", height: "250px", }, }} />
            <img src="images/2.jpeg" alt="Image 2" className="image-front" style={{ position: "absolute", top: "120px", left: "160px", zIndex: "0", width: "250px", maxWidth: "100%", "@media (maxWidth: 768px)": { top: "80px", left: "10px", transform: "translateX(-50%)", width: "200px", }, "@media (maxWidth: 576px)": { left: "20px", top: "10px", width: "150px", }, }} />
          </div>
        </div>
        <div className="col-lg-6 col-sm-12">
          {data2 && data2.length > 0 && (
            <div className="col-lg-12">
              <h4 className="text-center" id="subtitulo">Misión</h4>
              <p id="text1" className="text-center">{data2[0].mision}</p>
            </div>
          )}
          {data2 && data2.length > 0 && (
            <div className="col-sm-12">
              <h4 className="text-center" id="sub">Visión </h4>
              <p id="text1" className="text-center">{data2[1].mision}</p>
            </div>
          )}
          <h4 className="text-center" id="sub">
            Valores
          </h4>
          <ul className="column-list">
            {data3.map((valor, index) => (
              <li key={index}>
                <div className="item-container">
                  <h4 className="card-text4" style={{ fontWeight: 'bold', fontSize: '18px' }}> {valor.valor} </h4>
                  <p className="card-text2 text-justify pr-6"> {valor.descripcion} </p>
                </div>
              </li>
            ))}
          </ul>
        </div>
      </div>
    </div>
  );
}

export default Nosotros;
