import React, { Fragment, useEffect, useState } from 'react';
import Modal from 'react-modal';

function Buscador() {
  const [imageList, setImageList] = useState([]);
  const [color, setColor] = useState('');
  const [modalOpen, setModalOpen] = useState(false);
  const [currentImage, setCurrentImage] = useState(null);
  const colores = [
    { value: 'rojo carmesí', label: 'Rojo carmesí' },
    { value: 'azul holandés', label: 'Azul holandés' },
    { value: 'verde', label: 'Verde' },
    { value: 'amarillo tráfico', label: 'Amarillo tráfico' },
    { value: 'naranja', label: 'Naranja' },
    { value: 'negro brillante', label: 'Negro brillante' },
    { value: 'negro texturizado', label: 'Negro texturizado' },
    { value: 'blanco', label: 'Blanco' },
  ];

  useEffect(() => {
    let url = 'http://localhost:5000/gallery/color';
    if (color) {
      url += `?color=${color}`;
    }
    
    fetch(url)
      .then(res => res.json())
      .then(res => {
        const images = res.map(imagePath => {
          const imageName = imagePath.split('\\').pop(); // Obtener el nombre de archivo de la ruta
          return `http://localhost:5000/${imageName}`; // Construir la URL pública
        });
        setImageList(images);
      })
      .catch(err => {
        console.error(err);
      });
    
    console.log(setImageList);
  }, [color]);

  const modalHandler = (isOpen, image) => {
    setModalOpen(isOpen);
    setCurrentImage(image);
};

  return (
    <Fragment>
        <h4 className="text-center" id="subtitulo">Pintura Electrostática</h4>
        <div className='m-3' style={{ display: 'flex', justifyContent: 'flex-end'}}>
            <label htmlFor="color">Búsqueda por color:</label>
            <select className="form-select form-select-sm mb-2" aria-label=".form-select-sm"name="color" value={color} onChange={e => setColor(e.target.value)} style={{width:'210px'}}>
            <option value="">Seleccionar color</option>
            {colores.map(color => (
            <option key={color.value} value={color.value}>
                {color.label}
            </option>
            ))}
            </select>
        </div>
        <div className="mt-3" style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'center', alignItems: 'center', }}>
            {imageList.map(image => (
                <div key={image} className='card m-1' style={{ width: '320px',boxShadow: 'none', borderRadius: '0' }}>
                    <img src={image}className="card-img-top" style={{ height: '230px', width: '320px' }} onClick={() => modalHandler(true, image)}/>
                </div>
            ))}
        </div>
        <Modal style={{overlay: { zIndex: 9999, backgroundColor: 'rgba(0, 28, 48, .7)', display: 'flex', justifyContent: 'center', alignItems: 'center'}, content: { position: 'relative', top: 'auto', left: 'auto', right: 'auto', bottom: 'auto', border: 'none', borderRadius: 'none', maxWidth: '600px',  maxHeight: '90%',  padding: '0px',  }}} isOpen={modalOpen} onRequestClose={() => modalHandler(false, null)}>
          {currentImage && (
            <div className='card' style={{ borderRadius: '0', border: 'none' }}>
              <img src={currentImage} style={{ width: '100%', height: '400px' }} alt='Selected Image' />
            </div>
          )}
        </Modal>
    </Fragment>
  );
}

export default Buscador;
