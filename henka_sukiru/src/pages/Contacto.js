import React, {useRef, useState, useEffect} from 'react';
import './contacto.css'; 
import emailjs from '@emailjs/browser';
import Swal from 'sweetalert2';

const Contacto = ()=>{

    const formRef = useRef();
    const [data, setData] = useState([]);
    const [data1, setData1] = useState([]);

    useEffect(()=>{
        fetch('http://localhost:5000/info')
        .then(response => response.json())
        .then(data =>{
        console.log(data[0]);
        setData(data[0]);
        })
        .catch(err => console.log(err))

        fetch('http://localhost:5000/horario')
        .then(response => response.json())
        .then(data1 =>{
        setData1(data1);
        })
        .catch(err => console.log(err))
      }, []);

    const sendEmail = async (event) =>{
      event.preventDefault();

      const form = event.target;
      const name = form.user_name.value;
      const phone = form.user_phone.value;
      const email = form.user_email.value;
      const message = form.message.value;
      const service = form.service.value;

      const errors = {};
      if (name.trim() === '') {
          errors.name = 'El nombre es requerido';
      }
      if (phone.trim() === '') {
          errors.phone = 'El número de teléfono es requerido';
      }
      if (email.trim() === '') {
          errors.email = 'El correo electrónico es requerido';
      }
      if (service.trim() === '') {
        errors.service = 'El servicio es requerido';
      }
      if (message.trim() === '') {
          errors.message = 'El mensaje es requerido';
      }

      // Validar el correo electrónico
      const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
      if (email.trim() !== '' && !emailRegex.test(email)) {
          errors.email = 'El correo electrónico no es válido';
      }

      // Validar el número de teléfono
      const phoneRegex = /^\d{10}$/;
      if (phone.trim() !== '' && !phoneRegex.test(phone)) {
      errors.phone = 'El número de teléfono no es válido';
      }

      // Mostrar los errores si existen
      setFormErrors(errors);

      if (Object.keys(errors).length === 0) {
          emailjs.sendForm('service_tl63ith', 'template_7ufgyjo', formRef.current, 'tVhYQWJJSQvotEIRV')
          .then((response) => {
              console.log(response.text);
              formRef.current.reset(); // Restablecer los valores del formulario
              setFormErrors({}); // Limpiar los errores
              Swal.fire({
                  icon: 'success',
                  title: 'Envío exitoso',
                  showConfirmButton: false,
                  timer: 1500
                });
          })
          .catch((error) => console.log(error.text));
        } 
    }; 
    
    const [formErrors, setFormErrors] = useState({});


    return (
        <div className="container">
            <h4 className="text-center" id="subtitulo"></h4>
            <div className="container">
                <div className="row">
                    <div className="col-lg-6 col-sm-12">
                        <h4 className="" id="sub1">Contáctanos</h4>
                        <div className="info-form p-2">
                            <p id='text1'>{data.descripcion}</p>
                            <ul>
                                <li><a id='dis'><i className="fa fa-phone"></i>{data.numero}</a></li>
                                <li><a id='dis'><i className="fa fa-envelope"></i>{data.email}</a></li>
                            </ul>
                        </div>
                        <h5 id='sub'>Horarios</h5>
                    <ul>
                    {data1.map((horario, index) => {
                      return (
                        <li key={`horario-${index}`}>
                          <a>{horario.horario}</a>
                        </li>
                      );
                    })}
                    </ul>
                    </div>
                    <div className="col-lg-6 col-sm-12">
                        <form ref={formRef} onSubmit={sendEmail} style={{alignItems: 'center', width:'100%',height:'440px',boxShadow:'none',padding:'20px 30px',boxSizing:'border-box',position:'relative'}}>                           
                          <h4 className="" id="sub1">Cotiza con nosotros</h4> 
                          <input type="string" name="user_name"className={`form-control form-control-sm m-2 ${formErrors.name ? 'is-invalid' : ''}`} id="" placeholder="Nombre completo:"/>{formErrors.name && <div className="invalid-feedback">{formErrors.name}</div>}
                          <input type="number" name="user_phone" className={`form-control form-control-sm m-2 ${formErrors.phone ? 'is-invalid' : ''}`} id="" placeholder="Número de teléfono:"/> {formErrors.phone && <div className="invalid-feedback">{formErrors.phone}</div>}
                          <input type="email" name="user_email" className={`form-control form-control-sm m-2 ${formErrors.email ? 'is-invalid' : ''}`} id="" placeholder="Email:" /> {formErrors.email && <div className="invalid-feedback">{formErrors.email}</div>}
                          <input type="string" name="service" className={`form-control form-control-sm m-2 ${formErrors.service ? 'is-invalid' : ''}`} id="" placeholder="Servicio requerido:" /> {formErrors.service && <div className="invalid-feedback">{formErrors.service}</div>}
                          <textarea name="message" className={`form-control form-control-sm m-2 ${formErrors.message ? 'is-invalid' : ''}`} id=""rows="3" placeholder="Escribe el mensaje aquí"/> {formErrors.message && <div className="invalid-feedback">{formErrors.message}</div>}
                          <p className='field'>
                              <button className="btn btn-primary" id='button1' type="submit" value='Send'>Enviar</button>
                          </p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Contacto;