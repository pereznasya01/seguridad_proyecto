import React, { useEffect, useState } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import jwt_decode from 'jwt-decode';

const ProtectedRou = ({ canActivate, redirectPath = '/' }) => {
  const [isAuthenticated, setIsAuthenticated] = useState(false);
  const navigate = useNavigate();

  useEffect(() => {
    const token = localStorage.getItem('token');
    if (token) {
      try {
        const decodedToken = jwt_decode(token);
        const currentTime = Date.now() / 1000;
        if (decodedToken.exp > currentTime) {
          setIsAuthenticated(true);
        }
      } catch (error) {
        console.log('Error al decodificar el token:', error);
      }
    }
  }, []);

  if (!canActivate || !isAuthenticated) {
    return <Navigate to={redirectPath} replace />;
  }

  return navigate('/edicion');
}

export default ProtectedRou;