import React, { Fragment, useState, useEffect } from 'react';
import './pintura.css';

const Automati = () => {
    const [data, setData] = useState([]);
    const [data1, setData1] = useState([]);
    const [data3, setData3] = useState([]);
    
    useEffect(() => {

        fetch('http://localhost:5000/concepto0')
        .then(response => response.json())
        .then(data =>{
        console.log(data[2]);
        setData(data[2]);
        })
        .catch(err => console.log(err))

        fetch('http://localhost:5000/concepto3')
        .then(response => response.json())
        .then(datos =>{
        console.log(datos);
        setData1(datos);
        })
        .catch(err => console.log(err))

        fetch('http://localhost:5000/aplicacion-Auto')
        .then(response => response.json())
        .then(data3 => {
          console.log(data3);
          setData3(data3);
        })
        .catch(err => console.log(err));
      }, []);

      const limitedData = data1.slice(0, 4);
      const limitedDat = data1.slice(4, 7);

      const imagenes = [
        'images/meta.png',
        'images/alma.png',
        'images/medici.png',
        'images/energ.png',
        'images/elect.png'
      ];

  return (
    <Fragment>
        <div className="bgimg " id="home" style={{height:'300px', backgroundImage: 'url("images/20200625_203627.jpg")'}}>
            <div className="display-middle text-center">
                <span className="text-white fs-1">Automatización</span>
            </div>
        </div>
        <div className='container mt-3 mb-3'> 
            <p className="card-text">{data.descripcion && data.descripcion}</p>
        </div>
        <div id='rectan'>
        <h4 className="text-center" id="sub">Ventajas de la Automatización</h4>
            <div className="container dl-blurbs">
                <dl id='list'>
                {limitedData.map((valor, index) => {
                    return (
                        <React.Fragment key={index}>
                        <dt>{valor.titulo}</dt>
                        <dd>{valor.descripcion}</dd>
                        </React.Fragment>
                    );
                    })}
                    </dl>
                    <dl>
                    {limitedData.map((valor, index) => {
                        return (
                            <React.Fragment key={index}>
                            <dt>{valor.titulo}</dt>
                            <dd>{valor.descripcion}</dd>
                            </React.Fragment>
                        );
                        })}
                </dl>
            </div>
        </div>
        <div className='container'>
            <h4 className="text-center" id="sub">Aplicación de la Automatización</h4>
            <div className='col'>
                <div className='row'>
                    {data3.map((apli, index) => {
                        return (
                            <div className="col-md-4 p-2 text-center" key={index}>
                                <img src={imagenes[index]} className="card-img-top rounded-circle mx-auto d-block" alt={`Imagen ${index + 1}`} style={{ height: '200px', width: '200px', alignItems: 'center' }}/>
                                <div className="text-center">
                                    <h5 className='card-text3'>{apli.aplicacion}</h5>
                                    <p className="card-text2 text-center pr-6">{apli.descripcion}</p>
                                </div>
                            </div>  
                        );
                    })} 
                </div>
            </div>
        </div>
    </Fragment>
  )
}

export default Automati;