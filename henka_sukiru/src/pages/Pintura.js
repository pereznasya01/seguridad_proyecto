import React, { Fragment, useState, useEffect } from 'react';
import './pintura.css';
import Entrada from './Entrada';

const Pintura = () => {
    const [data, setData] = useState([]);
    const [data2, setData2] = useState([]);
    
    useEffect(() => {

        fetch('http://localhost:5000/concepto0')
        .then(response => response.json())
        .then(data =>{
        console.log(data[0]);
        setData(data[0]);
        })
        .catch(err => console.log(err))

        fetch('http://localhost:5000/aplicacion-Pin')
        .then(response => response.json())
        .then(data2 => {
          console.log(data2);
          setData2(data2);
        })
        .catch(err => console.log(err));
      }, []);

      const imagenes = [
        'images/const.png',
        'images/meta.png',
        'images/arqui.png',
        'images/mobili.png',
        'images/elect.png',
        'images/manuf.png'
      ];

  return (
    <Fragment>
        <div className="bgimg " id="home" style={{height:'300px', backgroundImage: 'url("images/pinturita.jpeg")'}}>
            <div className="display-middle text-center">
                <span className="text-white fs-1">Pintura Electrostática</span>
            </div>
        </div>
        <div className='container mt-3 mb-3'> 
            <p className="card-text">{data.descripcion && data.descripcion}</p>
        </div>
        <div id='rectan'>
            <Entrada />
        </div>
        <div className='container'>
            <h4 className="text-center" id="sub">Aplicación de la pintura Electrostática</h4>
                <div className='row'>
                {data2.map((apli, index) => {
                    return (
                        <div className="col-md-4 p-2 text-center" key={index}>
                            <img src={imagenes[index]} className="card-img-top rounded-circle mx-auto d-block" alt={`Imagen ${index + 1}`} style={{ height: '200px', width: '200px', alignItems: 'center' }}/>
                            <div className="text-center">
                                <h5 className='card-text3'>{apli.aplicacion}</h5>
                                <p className="card-text2 text-center pr-6">{apli.descripcion}</p>
                            </div>
                        </div>  
                    );
                })} 
                </div>
            
        </div> 
    </Fragment>
  )
}

export default Pintura;