import React, { Fragment, useEffect, useState } from "react";
import Modal from 'react-modal';

const Logs = () => {
    const [data, setData] = useState([]);
    const [modalOpen, setModalOpen] = useState(false);

    useEffect(() => {
        fetch('http://localhost:5000/logs')
            .then(response => response.json())
            .then(data => {
                console.log(data);
                setData(data);
            })
            .catch(err => console.log(err))
    }, []);

    const handleLoginClick = () => {
        setModalOpen(true);
    }

    const buttonStyle = {
        position: 'fixed',
        top: '30%',
        width: '60px',
        right: 10,
        backgroundColor: 'rgb(143, 10, 10)',
        transform: 'translateY(-100%)',
        fontSize: '20px',
    };

    const modalStyle = {
        content: {
            width: '80%',
            height: '70%', 
            top: '55%',
            left: '50%',
            transform: 'translate(-50%, -50%)',
        },
        overlay: {
            backgroundColor: 'rgba(0, 0, 0, 0.5)' // Fondo semi-transparente
        }
    };

    return (
        <Fragment>
             <div className="logs-container">
                <button className="btn p-1 m-1 text-white"  style={buttonStyle} onClick={handleLoginClick}> LOGS</button>
            </div>
            <Modal style={modalStyle} isOpen={modalOpen} onRequestClose={() => setModalOpen(false)}>
                <div>
                    <h2>Logs</h2>
                    <table className="table table-bordered">
                        <thead>
                            <tr>
                                <th>Url</th>
                                <th>Method</th>
                                <th>Timestamp</th>
                                <th>Content Type</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            {data && data.map((item, index) => (
                                <tr key={index}>
                                    <td id="foot1">{item.url}</td>
                                    <td id="foot1">{item.method}</td>
                                    <td id="foot1">{item.timestamp}</td>
                                    <td id="foot1">{item.contentType}</td>
                                    <td id="foot1">{item.descripcion}</td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </Modal>
        </Fragment>
    );
}

export default Logs;
