import React, { Fragment, useEffect, useState } from 'react';
import Modal from 'react-modal';

const Edicion5 = () => {
  const [data, setData] = useState([]);
  const [modalOpen, setModalOpen] = useState(false);
  const [id, setId] = useState('');
  const [valor, setValor] = useState('');
  const [editValor, setEditValor] = useState('');
  const [desc, setDesc] = useState('');
  const [editedDesc, setEditedDesc] = useState('');

  useEffect(() => {
    fetch('http://localhost:5000/valores')
      .then(response => response.json())
      .then(data => {
        console.log(data);
        setData(data);
      })
      .catch(err => console.log(err));
  }, []);

  const handleLoginClick = valores => {
    setId(valores.id);
    setValor(valores.valor);
    setEditValor(valores.valor);
    setDesc(valores.descripcion);
    setEditedDesc(valores.descripcion);
    setModalOpen(true);
  };

  const actualizarInformacion = e => {
    e.preventDefault();

    fetch('http://localhost:5000/update6', {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ id: id, valor: editValor, descripcion: editedDesc }),
    })
      .then(response => {
        if (!response.ok) {
          throw new Error('Error de servidor');
        }
        return response.json();
      })
      .then(data => {
        console.log(data);
        setValor(editValor);
        setDesc(editedDesc);
        window.location.reload();
      })
      .catch(err => console.log('Error al actualizar ' + err));
  };

  const handleChangeVa = e => {
    setEditValor(e.target.value);
  };

  const handleChange = e => {
    setEditedDesc(e.target.value);
  };

  return (
    <Fragment>
      <h4 id='sub'>Valores</h4>
      <div>
        <table className='table table-bordered table-responsive'>
          <thead>
            <tr>
              <th>Id</th>
              <th>Horario</th>
              <th>Descripción</th>
              <th>Acción</th>
            </tr>
          </thead>
          <tbody>
            {data.map((valores, index) => {
              return (
                <tr key={index}>
                  <td id='foot1'>{valores.id}</td>
                  <td id='foot1'>{valores.valor}</td>
                  <td id='foot1'>{valores.descripcion}</td>
                  <td>
                    <button id='foot1' className='btn btn-dark p-1 m-1 h8' onClick={() => handleLoginClick(valores)}>
                      Editar
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
      <Modal className='custom-modal2' overlayClassName='custom-modal-overlay2' isOpen={modalOpen} onRequestClose={() => setModalOpen(false)} appElement={document.getElementById('root')}>
        <div id='inicio_ses'>
          <form className='form' onSubmit={actualizarInformacion}>
            <h2 className='text-left'>Editar información</h2>
            <div className='form-group1'>
              <label id='Id2'>Id:{id}</label>
              <label id='Id2'>Valores</label>
              <input type='text' id='foot1' name='titulo' value={editValor} onChange={handleChangeVa} />
              <label id='Id2'>Descripción</label>
              <textarea className='textarea-full-width'type='text'id='foot1'name='descripcion' value={editedDesc} onChange={handleChange} style={{ width: '120%' }}></textarea>
            </div>
            <br></br>
            <div className='form-group1'>
              <button type='submit'>Editar</button>
            </div>
          </form>
        </div>
      </Modal>
    </Fragment>
  );
};

export default Edicion5;