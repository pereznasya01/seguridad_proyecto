import React, { Fragment, useEffect, useState } from 'react';
import Modal from 'react-modal';

const Edicion3 = () => {
  const [data, setData] = useState([]);
  const [modalOpen, setModalOpen] = useState(false);
  const [id, setId] = useState('');
  const [misi, setMisi] = useState('');
  const [editedMisi, setEditedMisi] = useState('');

  useEffect(() => {
    fetch('http://localhost:5000/mision')
      .then(response => response.json())
      .then(data => {
        console.log(data);
        setData(data);
      })
      .catch(err => console.log(err));
  }, []);

  const handleLoginClick = mision => {
    setId(mision.id);
    setMisi(mision.mision);
    setEditedMisi(mision.mision);
    setModalOpen(true);
  };

  const actualizarInformacion = e => {
    e.preventDefault();

    fetch('http://localhost:5000/update4', {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ id: id, mision: editedMisi }),
    })
      .then(response => {
        if (!response.ok) {
          throw new Error('Error de servidor');
        }
        return response.json();
      })
      .then(data => {
        console.log(data);
        setMisi(editedMisi);
        window.location.reload();
      })
      .catch(err => console.log('Error al actualizar ' + err));
  };

  const handleChange = e => {
    setEditedMisi(e.target.value);
  };

  return (
    <Fragment>
      <h4 id='sub'>Misión y Visión</h4>
      <div>
        <table className='table table-bordered table-responsive'>
          <thead>
            <tr>
              <th></th>
              <th>Id</th>
              <th>Titulo</th>
              <th>Acción</th>
            </tr>
          </thead>
          <tbody>
            {data.map((mision, index) => {
                let title;
                if (index === 0) {
                  title = 'Misión';
                } else if (index === 1) {
                  title = 'Visión';
                }

              return (
                <tr key={index}>
                  <td id='foot1'>{title}</td>
                  <td id='foot1'>{mision.id}</td>
                  <td id='foot1'>{mision.mision}</td>
                  <td>
                    <button id='foot1' className='btn btn-dark p-1 m-1 h8' onClick={() => handleLoginClick(mision)}>
                      Editar
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
      <Modal className='custom-modal1' overlayClassName='custom-modal-overlay1' isOpen={modalOpen} onRequestClose={() => setModalOpen(false)} appElement={document.getElementById('root')}>
        <div id='inicio_ses'>
          <form className='form' onSubmit={actualizarInformacion}>
            <h2 className='text-left'>Editar información</h2>
            <div className='form-group1'>
              <label id='Id2'>Id:{id}</label>
              <label id='Id2'>Descripción</label>
              <textarea className='textarea-full-width'type='text'id='foot1'name='descripcion' value={editedMisi} onChange={handleChange} style={{ width:'100%'}}></textarea>
            </div>
            <div className='form-group1'>
              <button type='submit'>Editar</button>
            </div>
          </form>
        </div>
      </Modal>
    </Fragment>
  );
};

export default Edicion3;
