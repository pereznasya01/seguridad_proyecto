import React, {useState, Fragment, useEffect} from 'react';
import './proyecto.css';
import Modal from 'react-modal';
import Swal from 'sweetalert2';

const Galeria = ()=>{

    const [file, setFile] = useState(null)
    const [imageList, setImageList] = useState([])
    const [listUpdated, setListUpdated] = useState(false)
    const [modalOpen, setModalOpen] = useState(false)
    
    const [currentImage, setCurrentImage] = useState(null)

    useEffect(() => {

        Modal.setAppElement('body')

        fetch('http://localhost:5000/images/get')
        .then(res => res.json())
        .then(res => setImageList(res))
        .catch(err =>{
            console.error(err)
        }) 
        setListUpdated(false)
        console.log(setImageList)
    }, [listUpdated])

    

    const selectHandler = e =>{
        setFile(e.target.files[0])
    }

    const sendHandler = () =>{
        if(!file){
            Swal.fire({
                icon: 'error',
                title: 'Debes seleccionar un archivo para subir',
                showConfirmButton: false,
                timer: 1500
              });
            return
        }

        const formdata = new FormData()
        formdata.append('image', file)

        fetch('http://localhost:5000/images/post', {
            method: 'POST',
            body: formdata
        })
        .then(res => res.text())
        .then(res => {
            console.log(res)
            setListUpdated(true)
        })
        .catch(err =>{
            console.error(err)
        })

        //No se queda guardada la imagen
        setFile(null)
        document.getElementById('formFileSm').value = null;
    }

    const modalHandler = (isOpen, image) => {
        setModalOpen(isOpen);
        setCurrentImage(image);
    };

    const deleteHandler = ()=>{

        let imageId = currentImage.split('-') 
        imageId = parseInt(imageId[0])

        fetch('http://localhost:5000/images/delete/'+ imageId, {
            method: 'DELETE'
        })
        .then(res => res.text())
        .then(res => console.log(res))

        setModalOpen(false)
        setListUpdated(true)
    }

    return (
        <Fragment>
            <div className="container">
                <h4 className="text-center" id="sub">Proyectos</h4>
                <div className="container">
                    <div className="mb-3">
                        <div className="row">
                            <div className="col-10">
                                <input  onChange={selectHandler} className="form-control form-control-sm" id="formFileSm" type="file"></input>
                            </div>
                            <div className="col-2">
                                <button onClick={sendHandler} type="button" className="btn btn-dark btn-sm col-12">Enviar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className='mt-3' style={{display:'flex', flexWrap:"wrap", justifyContent: 'center', alignItems: 'center'}}>
                    {imageList.map(image =>(
                        <div key={image} className='card m-2'>
                            <img src={'http://localhost:5000/'+ image} className="card-img-top" style={{height:'230px', width:'320px'}} onClick={()=> modalHandler(true, image)}></img>
                        </div>
                    ))}
            </div>

            <Modal style={{overlay: { zIndex: 9999, backgroundColor: 'rgba(255, 255, 255, 0.5)', display: 'flex', justifyContent: 'center', alignItems: 'center', }, content: { position: 'relative', top: 'auto', left: 'auto', right: 'auto', bottom: 'auto', border: 'none', borderRadius: '8px', maxWidth: '600px', maxHeight: '90%', padding: '20px', }}} isOpen={modalOpen} onRequestClose={() => modalHandler(false, null)}>
                <div className='card'>
                    <img src={'http://localhost:5000/'+ currentImage} style={{width:'100%', height: '400px'}}></img>
                    <div className='card-body'>
                        <button onClick={()=> deleteHandler()} style={{marginTop:0}} className='btn btn-danger'>Eliminar</button>
                    </div>
                </div>
            </Modal>
        </Fragment>
    )
}

export default Galeria;