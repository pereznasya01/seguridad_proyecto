import React, {Fragment, useEffect, useState} from 'react';
import './home.css';
import Entrada from './Entrada';

const Home = ()=>{
    const [data, setData] = useState([]);
    
    useEffect(() => {

        fetch('http://localhost:5000/concepto0')
        .then(response => response.json())
        .then(data =>{
        console.log(data[0]);
        setData(data[0]);
        })
        .catch(err => console.log(err))

      }, []);

        return (
        <Fragment>
            <div className="container py-md-5">
            <div className="card-1">
                <div className="row ">
                <div className="col-sm-12 col-md-7 px-3">
                    <h4 className="card-title1 text-center">Pintura Electrostática</h4>
                    <p className="card-text">{data.descripcion && data.descripcion}</p>
                </div>
                <div className="col-sm col-md-5 position-relative">
                    <img className="d-block w-100" src="images/aplicacion pintura.jpeg" alt=""></img>
                </div>
                </div>       
            </div>
            </div>
          <Entrada />
        </Fragment>
    )
}

export default Home;