import React, { useEffect, useState } from 'react';
import './footer.css';
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUser } from '@fortawesome/free-solid-svg-icons';
import Modal from 'react-modal';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';

const Footer = () => {
  const [values, setValues] = useState({
    username: '',
    password: ''
  });
  const [data, setData] = useState([]);
  const navigate = useNavigate();

  useEffect(() => {
    fetch('http://localhost:5000/info')
      .then(response => response.json())
      .then(data => {
        console.log(data[0]);
        setData(data[0]);
      })
      .catch(err => console.log(err))
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();

    fetch('http://localhost:5000/login', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(values)
    })
      .then(res => res.json())
      .then(data => {
        if (data.token) {
          localStorage.setItem('token', data.token);
          navigate('/edicion');
          setModalOpen(false);
        } else {
          Swal.fire({
            icon: 'error',
            title: 'Error',
            showConfirmButton: false,
            timer: 1500
          });
        }
      })
      .catch(err => console.log(err));
  };

  const handleChange = (e) => {
    setValues({ ...values, [e.target.name]: e.target.value });
  };

  const [modalOpen, setModalOpen] = useState(false);

  const handleLoginClick = () => {
    setModalOpen(true);
  };

  const [trimmedDescripcion, setTrimmedDescripcion] = useState('');

  useEffect(() => {
    if (data.descripcion) {
      const palabras = data.descripcion.split(' ');
      const primeras40Palabras = palabras.slice(0, 39).join(' ');
      setTrimmedDescripcion(primeras40Palabras);
    }
  }, [data.descripcion]);

  useEffect(() => {
    const token = localStorage.getItem('token');
    if (token) {
      navigate('/edicion');
    }
  }, []);

  return (
    <footer className="footer-01">
      <div className="container">
        <div className="row">
          <div className="col-md-6 col-lg-3 mb-md-6">
            <h2 className="footer-heading">Henka Sukiru</h2>
            <p id="foot">{trimmedDescripcion}</p>
          </div>
          <div className="col-md-6 col-lg-3 pl-lg-5 mb-md-6">
            <h2 className="footer-heading">Páginas</h2>
            <ul className="list-unstyled">
              <li><Link to='/'>Home</Link></li>
              <li><Link to='/servicio' className="py-1 d-block">Servicios</Link></li>
              <li><Link to='/nosotros' className="py-1 d-block">Nosotros</Link></li>
              <li><Link to='/proyecto' className="py-1 d-block">Proyectos</Link></li>
              <li><Link to='/contacto' className="py-1 d-block">Contacto</Link></li>
              <li onClick={handleLoginClick}><FontAwesomeIcon icon={faUser} /></li>
            </ul>
          </div>
          <div className="col-md-6 col-lg-3 mb-md-6">
            <h2 className="footer-heading">¿Tienes preguntas?</h2>
            <div className="block-23 mb-3">
              <ul>
                <li id="foot"><span className="icon"></span>{data.ubicacion}</li>
                <li id="foot">{data.numero}</li>
                <li id="foot"><span className="icon"></span>{data.email}</li>
              </ul>
            </div>
          </div>
          <div className="col-md-6 col-lg-3 mb-md-6">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3702.5139970055607!2d-102.33526822589116!3d21.87627435813342!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8429eea5a821f10d%3A0x3930a47cf8898f23!2sAmeca%20108%2C%20La%20Soledad%2C%2020326%20Aguascalientes%2C%20Ags.!5e0!3m2!1ses!2smx!4v1687795378030!5m2!1ses!2smx" style={{ height: '240px', width: '350px' }} allowFullScreen="" loading="lazy" referrerPolicy="no-referrer-when-downgrade"></iframe>
          </div>
        </div>
      </div>
      <Modal className="custom-modal" overlayClassName="custom-modal-overlay" isOpen={modalOpen} onRequestClose={() => setModalOpen(false)}>
        <div id='inicio_ses'>
          <h2 >Inicio de sesión</h2>
          <form className='form' onSubmit={handleSubmit}>
            <div className="form-group1">
              <label htmlFor="username">Usuario:</label>
              <input type="text" name='username' placeholder="Ingresa tu nombre de usuario" required value={values.username} onChange={handleChange}></input>
            </div>
            <div className="form-group1">
              <label htmlFor="password">Contraseña:</label>
              <input type="password" name='password' placeholder="Ingresa tu contraseña" required value={values.password} onChange={handleChange}></input>
            </div>
            <div className="form-group1">
              <button type="submit">Iniciar sesión</button>
            </div>
          </form>
        </div>
      </Modal>
    </footer>
  );
}

export default Footer;
