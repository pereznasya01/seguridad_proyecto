import React, { Fragment, useState }  from 'react';
import './login.css';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';

const Login = () => {

    const [values, setValues] = useState({
        username: '',
        password: ''
      });
    
      const navigate = useNavigate();
    
      const handleSubmit = (e) => {
        e.preventDefault();
    
        fetch('http://localhost:5000/login', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(values)
        })
          .then(res => res.json())
          .then(data => {
            if (data.token) {
                localStorage.setItem('token', data.token); 
                navigate('/edicion');
                
            } else {
              Swal.fire({
                icon: 'error',
                title: 'Error',
                showConfirmButton: false,
                timer: 1500
              });
            }
          })
          .catch(err => console.log(err));
      };
    
      const handleChange = (e) => {
        setValues({ ...values, [e.target.name]: e.target.value });
      };

    return(
        <Fragment>
            <div id='inicio_ses'>
                <h2 >Inicio de sesión</h2>
                <form className='form' onSubmit={handleSubmit}>
                    <div class="form-group1">
                        <label for="username">Usuario:</label>
                        <input type="text" name='username' placeholder="Ingresa tu nombre de usuario" required value={values.username} onChange={handleChange}></input>
                    </div>
                    <div className="form-group1">
                        <label for="password">Contraseña:</label>
                        <input type="password" name='password' placeholder="Ingresa tu contraseña" required value={values.password} onChange={handleChange}></input>
                    </div>
                    <div className="form-group1">
                        <button type="submit">Iniciar sesión</button>
                    </div>
                </form>
            </div>
        </Fragment>
    )
}

export default Login;
