import React, { Fragment, useEffect, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import './editable.css';
import Modal from 'react-modal';
import Galeria from './Galeria';
import Edicion_info from './Edicion_info';
import Edicion1 from './Edicion1';
import Edicion0 from './Edicion0';
import Edicion2 from './Edicion2';
import Edicion3 from './Edicion3';
import Edicion4 from './Edicion4';
import Edicion5 from './Edicion5';
import Edicion6 from './Edicion6';
import Edicion7 from './Edicion7';
import Edicion8 from './Edicion8';
import Galeria_nom from './Galeria_nom';
import Logs from './Logs';
const Editable = () => {
  const [data, setData] = useState([]);
  const [selectedDescripcion, setSelectedDescripcion] = useState('');
  const [selectedId, setSelectedId] = useState('');
  const [editedDescripcion, setEditedDescripcion] = useState('');
  const navigate = useNavigate();
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  useEffect(() => {
    const token = localStorage.getItem('token');
    if (token) {
      setIsLoggedIn(true);
    }
    else{
      navigate('/');
    }

    fetch('http://localhost:5000/concepto0')
      .then(response => response.json())
      .then(data => {
        console.log(data);
        setData(data);
      })
      .catch(err => console.log(err));
  }, [navigate]);

  const [modalOpen, setModalOpen] = useState(false);

  const handleLoginClick = descripcion => {
    setSelectedDescripcion(descripcion.descripcion);
    setSelectedId(descripcion.id);
    setEditedDescripcion(descripcion.descripcion);
    setModalOpen(true);
  };

  const actualizarInformacion = e => {
    e.preventDefault();

    fetch('http://localhost:5000/update0', {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ id: selectedId, descripcion: editedDescripcion })
    })
      .then(response => {
        if (!response.ok) {
          throw new Error('Error de servidor');
        }
        return response.json();
      })
      .then(data => {
        console.log(data);
        setSelectedDescripcion(editedDescripcion); 
        window.location.reload();
      })
      .catch(err => console.log('Error al actualizar ' + err));
  };

  const handleChangeDescripcion = e => {
    setEditedDescripcion(e.target.value);
  };

  // const eliminarTexto= (id) =>{

  //   fetch('http://localhost:5000/delete0/'+ id, {
  //     method: 'DELETE'
  //   })
  //   .then(res => window.location.reload()
  //   )
  // }

  const handleLogout = () => {
    localStorage.removeItem('token');
    navigate('/');
  };

  return isLoggedIn ? (
    <Fragment>
      <nav className="shadow-lg header navbar navbar-expand-lg">
        <div className="container-fluid">
          <button
            className="navbar-toggler alert-primary text-dark"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarNav"
            aria-controls="navbarNav"
            aria-expanded="true"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarNav">
            <ul className="navbar-nav nav-links ml-auto">
              <li className='text-white' onClick={handleLogout}>Logout</li>
            </ul>
          </div>
        </div>
      </nav>
      <div className="container">
        <div>
          <h1 className="text-center" id="subtitulo">
            Apartado Edicion
          </h1>
          <h4 id="sub">Apartado Inicio</h4>
          <table className="table table-bordered">
            <thead>
              <tr>
                <th>Id</th>
                <th>Información</th>
                <th>Acción</th>
              </tr>
            </thead>
            <tbody>
              {data.map((descripcion, index) => {
                return (
                  <tr key={index}>
                    <td id="foot1">{descripcion.id}</td>
                    <td id="foot1">{descripcion.descripcion}</td>
                    <td>
                      <button
                        id="foot1"
                        className="btn btn-dark p-1 m-1 h8"
                        onClick={() => handleLoginClick(descripcion)}
                      >
                        Editar
                      </button>
                      {/* <button id='foot1' className='btn btn-dark p-1 h8' onClick={() => eliminarTexto(descripcion.id)}>Eliminar</button> */}
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
        <Edicion_info />
        <Edicion1 />
        <Edicion0 />
        <Edicion2 />
        <div className="row">
          <div className="col-lg-4 col-sm-12">
            <Edicion4 />
          </div>
          <div className="col-lg-8 col-sm-12">
            <Edicion5 />
          </div>
          <div className="col-lg-6 col-sm-12">
            <Edicion6 />
            <Edicion3 />
          </div>
          <div className="col-lg-6 col-sm-12">
            <Edicion7 />
          </div>
        </div>
        <Edicion8 />
        <Galeria />
        <Galeria_nom />
        <Logs />
        <Modal
          className="custom-modal1"
          overlayClassName="custom-modal-overlay1"
          isOpen={modalOpen}
          onRequestClose={() => setModalOpen(false)}
        >
          <div id="inicio_ses">
            <h2>Editar información</h2>
            <form className="form" onSubmit={actualizarInformacion}>
              <div className="form-group1">
                <label id="Id2">Id:{selectedId}</label>
                <label id="Id2">Información</label>
                <textarea
                  type="text"
                  id="foot1"
                  className="textarea-full-width"
                  name="descripcion"
                  value={editedDescripcion}
                  onChange={handleChangeDescripcion}
                ></textarea>
              </div>
              <div className="form-group1">
                <button type="submit">Editar</button>
              </div>
            </form>
          </div>
        </Modal>
      </div>
    </Fragment>
  ): null;
}

export default Editable;
