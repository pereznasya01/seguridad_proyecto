import {Link, Outlet} from "react-router-dom";

const Layout = () =>{
    return <div>
        <header className="">
            <nav className="shadow-lg rounded header navbar navbar-expand-lg">
                <div className="container-fluid">
                <div className="logo">
                    <img src="images/henka.png" alt="Henka Sukiru"></img>
                </div>
                <button className="navbar-toggler alert-primary text-dark" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="true" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav nav-links ml-auto">
                    <li><Link to='/'>Home</Link></li>
                    <li><Link to='/servicio'>Servicios</Link></li>
                    <li><Link to='/nosotros'>Nosotros</Link></li>
                    <li><Link to='/proyecto'>Proyectos</Link></li>
                    <li><Link to='/contacto'>Contacto</Link></li>
                    </ul>
                </div>
                </div>
            </nav>
        </header>
        <Outlet />
    </div>
}

export default Layout;