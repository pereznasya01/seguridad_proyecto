
import React, { Fragment, useEffect, useState } from 'react';
import Modal from 'react-modal';

const Edicion2 = () => {
  const [data, setData] = useState([]);
  const [modalOpen, setModalOpen] = useState(false);
  const [id, setId] = useState('');
  const [desc, setDesc] = useState('');
  const [editedDesc, setEditedDesc] = useState('');
  const [titu, setTitu] = useState('');
  const [editedTitu, setEditedTitu] = useState('');

  useEffect(() => {
    fetch('http://localhost:5000/concepto3')
      .then(response => response.json())
      .then(data => {
        console.log(data);
        setData(data);
      })
      .catch(err => console.log(err));
  }, []);

  const handleLoginClick = ventaja => {
    setId(ventaja.id);
    setEditedDesc(ventaja.descripcion);
    setDesc(ventaja.descripcion);
    setTitu(ventaja.titulo);
    setEditedTitu(ventaja.titulo);
    setModalOpen(true);
  };

  const actualizarInformacion = e => {
    e.preventDefault();

    fetch('http://localhost:5000/update3', {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ id: id, descripcion: editedDesc, titulo: editedTitu }),
    })
      .then(response => {
        if (!response.ok) {
          throw new Error('Error de servidor');
        }
        return response.json();
      })
      .then(data => {
        console.log(data);
        setDesc(editedDesc);
        setTitu(editedTitu);
        window.location.reload();
      })
      .catch(err => console.log('Error al actualizar ' + err));
  };

  const handleChangeDes = e => {
    setEditedDesc(e.target.value);
  };

  const handleChangeTitu = e => {
    setEditedTitu(e.target.value);
  };

  return (
    <Fragment>
      <h4 id='sub'>Ventajas de la Automatización</h4>
      <div>
        <table className='table table-bordered table-responsive'>
          <thead>
            <tr>
              <th>Id</th>
              <th>Titulo</th>
              <th>Descripción</th>
              <th>Acción</th>
            </tr>
          </thead>
          <tbody>
            {data.map((ventaja, index) => {
              return (
                <tr key={index}>
                  <td id='foot1'>{ventaja.id}</td>
                  <td id='foot1'>{ventaja.titulo}</td>
                  <td id='foot1'>{ventaja.descripcion}</td>
                  <td>
                    <button id='foot1' className='btn btn-dark p-1 m-1 h8' onClick={() => handleLoginClick(ventaja)}>
                      Editar
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
      <Modal className='custom-modal2' overlayClassName='custom-modal-overlay1' isOpen={modalOpen} onRequestClose={() => setModalOpen(false)} appElement={document.getElementById('root')}>
        <div id='inicio_ses'>
          <form className='form' onSubmit={actualizarInformacion}>
            <h2 className='text-left'>Editar información</h2>
            <div className='form-group2'>
              <label id='Id2'>Id:{id}</label>
              <label id='Id2'>Titulo</label>
              <input type='text' id='foot1' name='titulo' value={editedTitu} onChange={handleChangeTitu} />
              <label id='Id2'>Descripción</label>
              <textarea className='textarea-full-width'type='text'id='foot1'name='descripcion' value={editedDesc} onChange={handleChangeDes} style={{ width: '120%' }}></textarea>
            </div>
            <div className='form-group1'>
              <button type='submit'>Editar</button>
            </div>
          </form>
        </div>
      </Modal>
    </Fragment>
  );
};

export default Edicion2;
