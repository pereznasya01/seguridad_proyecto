import React, {useState, Fragment, useEffect} from 'react';
import Modal from 'react-modal';
import Buscador from './Buscador';

const Proyectos = ()=>{

    const [imageList, setImageList] = useState([])
    const [listUpdated, setListUpdated] = useState(false)
    const [modalOpen, setModalOpen] = useState(false)
    
    const [currentImage, setCurrentImage] = useState(null)

    useEffect(() => {

        Modal.setAppElement('body')

        fetch('http://localhost:5000/images/get')
        .then(res => res.json())
        .then(res => setImageList(res))
        .catch(err =>{
            console.error(err)
        }) 
        setListUpdated(false)
    }, [listUpdated])


    const modalHandler = (isOpen, image) => {
        setModalOpen(isOpen);
        setCurrentImage(image);
    };

    return (
        <Fragment>
            <h4 className="text-center" id="subtitulo">Galería</h4>
            <div className='mt-3' style={{ display: 'flex', flexWrap: 'wrap',justifyContent: 'center', alignItems: 'center' }}>
                {imageList.map(image => (
                <div key={image} className='card m-1' style={{ width: '320px',boxShadow: 'none', borderRadius: '0' }}>
                    <img src={'http://localhost:5000/' + image} className='card-img-top' style={{ height: '230px', width: '100%', objectFit: 'cover',  }} onClick={() => modalHandler(true, image)} />
                </div>
                ))}
            </div>

            <Modal style={{overlay: { zIndex: 9999, backgroundColor: 'rgba(0, 28, 48, .7)', display: 'flex', justifyContent: 'center', alignItems: 'center'}, content: { position: 'relative', top: 'auto', left: 'auto', right: 'auto', bottom: 'auto', border: 'none', borderRadius: 'none', maxWidth: '600px',  maxHeight: '90%',  padding: '0px', 
                }}} isOpen={modalOpen} onRequestClose={() => modalHandler(false, null)}>
                <div className='card' style={{borderRadius: '0', border: 'none'}}>
                    <img src={'http://localhost:5000/'+ currentImage} style={{width:'100%', height: '400px'}}></img>
                </div>
            </Modal>
            <Buscador />
        </Fragment>
    )
}

export default Proyectos;