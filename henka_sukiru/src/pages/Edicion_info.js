import React, { Fragment, useEffect, useState } from 'react';
import './edicion.css';
import Modal from 'react-modal';

const Edicion_info = () => {
    const [data1, setData1] = useState([]);
    const [selId, setSelId] = useState('');
    const [selUbi, setSelUbi] = useState('');
    const [selUNum, setSelNum] = useState();
    const [selUEmail, setSelEmail] = useState('');
    const [selDes, setSelDes] = useState('');
    const [editUbi, editSetUbi] =  useState('');
    const [editNum, editSetNum] =  useState();
    const [editEma, editSetEma] =  useState('');
    const [editDes, editSetDes] = useState('');
    const [modalOpen, setModalOpen] = useState(false);

  useEffect(() => {
    fetch('http://localhost:5000/info')
      .then(response => response.json())
      .then(data => {
        console.log(data);
        setData1(data);
      })
      .catch(err => console.log(err));
  }, []);

  const handleLoginClick = info => {
    setSelId(info.id);
    setSelUbi(info.ubicacion);
    editSetUbi(info.ubicacion);
    setSelNum(info.numero);
    editSetNum(info.numero);
    setSelEmail(info.email);
    editSetEma(info.email);
    setSelDes(info.descripcion);
    editSetDes(info.descripcion);
    setModalOpen(true);
  }

  const actualizarInformacion = e => {
    e.preventDefault();

    fetch('http://localhost:5000/update', {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ id: selId, ubicacion: editUbi, numero: editNum, email: editEma, descripcion: editDes})
    })
      .then(response => {
        if (!response.ok) {
          throw new Error('Error de servidor');
        }
        return response.json();
      })
      .then(data => {
        console.log(data);
        setSelUbi(editUbi); 
        setSelNum(editNum);
        setSelEmail(editEma);
        setSelDes(editDes);
        window.location.reload();
      })
      .catch(err => console.log('Error al actualizar ' + err));
  };

  const handleChangeUbi = e => {
    editSetUbi(e.target.value);
  };
  const handleChangeNum = e => {
    editSetNum(e.target.value);
  };

  const handleChangeEma = e => {
    editSetEma(e.target.value);
  };

  const handleChangeDes = e =>{
    editSetDes(e.target.value);
  }

  return (
    <Fragment>
            <h4 id='sub'>Información empresa</h4>
            <div>
              <table className='table table-bordered table-responsive'>
                <thead>
                  <tr>
                    <th>Ubicación</th>
                    <th>Número</th>
                    <th>Correo</th>
                    <th>Descripción</th>
                    <th>Acción</th>
                  </tr>
                </thead>
                <tbody>
                  {data1.map((info, index) => {
                    return (
                      <tr key={index}>
                        <td id='foot1'>{info.ubicacion}</td>
                        <td id='foot1'>{info.numero}</td>
                        <td id='foot1'>{info.email}</td>
                        <td id='foot1'>{info.descripcion}</td>
                        <td>
                          <button id='foot1' className='btn btn-dark p-1 m-1 h8' onClick={() => handleLoginClick(info)}>Editar</button>
                        </td> 
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
        <Modal className='custom-modal2' overlayClassName='custom-modal-overlay2' isOpen={modalOpen} onRequestClose={() => setModalOpen(false)}>
          <div id='inicio_ses'>
            <form className='form' onSubmit={actualizarInformacion}>
            <h2 className='text-left'>Editar información</h2>
              <div className='form-group2'>
              <label htmlFor="id">Id:{selId}</label>
                <label id='Id2'>Ubicación</label>
                <input type='text' id='foot1' name='ubicacion' value={editUbi} onChange={handleChangeUbi}></input>
                <label id='Id2'>Número</label>
                <input type='text' id='foot1' name='numero' value={editNum} onChange={handleChangeNum}></input>
                <label id='Id2'>Email</label>
                <input type='text' id='foot1' name='email' value={editEma} onChange={handleChangeEma}></input>
                <label id='Id2'>Descripción</label>
                <textarea type='text' id='foot1' name='descripcion' value={editDes} onChange={handleChangeDes} style={{width:'120%'}}></textarea>
              </div>
              <div className='form-group1'>
                <button type='submit'>Editar</button>
              </div>
            </form>
          </div>
        </Modal>
    </Fragment>
  );
};

export default Edicion_info;