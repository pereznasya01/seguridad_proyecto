import React, {Fragment, useEffect, useRef, useState} from 'react';
import './home.css';

const Entrada = ()=>{
    const carruselRef = useRef(null);
    let maxScrollLeft =0;
    let intervalo = null;
    let step = 2;
    const [data1, setData1] = useState([]);
    
    useEffect(() => {
        fetch('http://localhost:5000/concepto1')
        .then(response => response.json())
        .then(datos =>{
        console.log(datos);
        setData1(datos);
        })
        .catch(err => console.log(err))

        const carrusel = carruselRef.current;
        maxScrollLeft = carrusel.scrollWidth - carrusel.clientWidth;
    
        const start = () => {
          intervalo = setInterval(() => {
            carrusel.scrollLeft = carrusel.scrollLeft + step;
            if (carrusel.scrollLeft === maxScrollLeft) {
              step = step * -1;
            } else if (carrusel.scrollLeft === 0) {
              step = step * -1;
            }
          }, 1);
        };
    
        const stop = () => {
          clearInterval(intervalo);
        };
    
        carrusel.addEventListener("mouseover", stop);
        carrusel.addEventListener("mouseout", start);
    
        start();
    
        return () => {
          carrusel.removeEventListener("mouseover", stop);
          carrusel.removeEventListener("mouseout", start);
          stop();
        };
      }, []);

      const limitedData = data1.slice(0, 3);
      const limitedDat = data1.slice(3, 5);

        return (
        <Fragment>
            <h4 className="text-center" id="sub">Ventajas de la pintura electrostática</h4>
            <div className="container dl-blurbs">
            <dl>
              {limitedData.map((valor, index) => {
                return (
                  <Fragment key={`data-${index}`}>
                    <dt>{valor.titulo}</dt>
                    <dd>{valor.descripcion}</dd>
                  </Fragment>
                );
              })}
            </dl>
            <dl>
              {limitedDat.map((valor, index) => {
                return (
                  <Fragment key={`dat-${index}`}>
                    <dt>{valor.titulo}</dt>
                    <dd>{valor.descripcion}</dd>
                  </Fragment>
                );
              })}
            </dl>
            </div>
            <h4 className="text-center" id="sub">Algunos proyectos</h4>
            <div className="carrusel">
                <div className="carrusel-items" ref={carruselRef}>
                    <div className="carrusel-item">
                    <img src="images/1.jpeg" alt="" />
                    </div>
                    <div className="carrusel-item">
                    <img src="images/2.jpeg" alt="" />
                    </div>
                    <div className="carrusel-item">
                    <img src="images/3.jpeg" alt="" />
                    </div>
                    <div className="carrusel-item">
                    <img src="images/4.jpeg" alt="" />
                    </div>
                    <div className="carrusel-item">
                    <img src="images/5.jpeg" alt="" />
                    </div>
                    <div className="carrusel-item">
                    <img src="images/6.jpeg" alt="" />
                    </div>
                    <div className="carrusel-item">
                        <img src="images/7.jpeg" alt="" />
                    </div>
                    <div className="carrusel-item">
                        <img src="images/8.jpeg" alt="" />
                    </div>
                    <div className="carrusel-item">
                        <img src="images/9.jpeg" alt="" />
                    </div>
                    <div className="carrusel-item">
                        <img src="images/10.jpeg" alt="" />
                    </div>
                    <div className="carrusel-item">
                        <img src="images/11.jpeg" alt="" />
                    </div>
                    <div className="carrusel-item">
                        <img src="images/12.jpeg" alt="" />
                    </div>
                </div>
            </div>
        </Fragment>
    )
}

export default Entrada;