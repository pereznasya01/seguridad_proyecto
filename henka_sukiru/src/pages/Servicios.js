import { Fragment } from "react";
import {Link} from "react-router-dom";
import './servicio.css';

const Servicios = ()=>{
    return (
        <Fragment>
            <h4 className="text-center" id="subtitulo">Servicios</h4>
            <div className="container">
                <div className="row">
                    <div className="col-sm-6 col-md-6 col-lg-4 mb-3">
                        <div className="card text-white card-has-bg" id="card1">
                            <Link to="/pintura-electrostatica" className="card text-white card-has-bg" id="card1">
                                <div className="content">
                                    <h2 className="card-title">Pintura Electrostática</h2>
                                </div>
                            </Link>
                        </div>
                    </div>
                    <div className="col-sm-6 col-md-6 col-lg-4 mb-3">
                        <div className="card text-white card-has-bg" id="card2">
                            <Link to="/automatizacion" className="card text-white card-has-bg" id="card2">    
                                <div className="content">
                                    <h2>Automatización</h2>
                                </div>
                            </Link>
                        </div>
                    </div>
                    <div className="col-sm-6 col-md-6 col-lg-4 mb-3">
                        <div className="card text-white card-has-bg" id="card3">
                            <Link to="/paileria" className="card text-white card-has-bg" id="card3">
                                <div className="content">
                                    <h2>Pailería</h2>
                                </div>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    )
}

export default Servicios;