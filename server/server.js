const express = require('express');
const app = express();
const mysql = require('mysql2');
const path = require('path'); 
const port = process.env.PORT || 5000;
const cors = require('cors');
const bodyParser = require('body-parser');

app.use(cors())

const mysqlConn = mysql.createConnection({ 
    host: 'localhost', 
    user: 'root', 
    password: 'Mew2604',
    database:'henka_database'
}); 

mysqlConn.connect(function (err){ 
    if (err){ 
        console.log(err.message); 
        return;
    }
    else{ 
        console.log('connection ok...'); 
    } 
}); 

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
// Configurar el middleware para servir las imágenes estáticas
app.use(express.static(path.join(__dirname, 'db_images')))
app.use(express.static(path.join(__dirname, 'db_imagenes')))
app.use(require('./routes/routes'));

app.listen(port, ()=>{
    console.log(`Server running in http://localhost:${port}`)
})