const express = require('express');
const app = express();
const router = express.Router();
const multer = require('multer');
const path = require('path'); 
const fs = require('fs'); 
const mysql = require('mysql2');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

const diskstorage = multer.diskStorage({
    destination: path.join(__dirname, '../images'),
    filename: (req, file, cb) => {
      cb(null, Date.now() + '-' + file.originalname);
    },
  });

  const diskStorageImagenes = multer.diskStorage({
    destination: path.join(__dirname, '../imagenes'),
    filename: (req, file, cb) => {
      cb(null, Date.now() + '-' + file.originalname);
    },
  });
  
  const fileUpload = multer({
    storage: diskstorage,
  }).single('image');

  const fileUploadImagenes = multer({
    storage: diskStorageImagenes,
  }).single('image');
  
  router.get('/', (req, res) => {
    res.send("Welcome to my app");
  });

const connection = mysql.createConnection({
    host: 'localhost', 
    user: 'root', 
    password: 'Mew2604', 
    database:'henka_database'
});

connection.connect((err) => {
  if (err) {
    console.error('Error al conectar a la base de datos:', err);
    return res.status(500).send('Error de servidor');
  }
  
  router.post('/images/post', fileUpload, async (req, res) => {
    try {
        if (!req.file || !req.file.mimetype.startsWith('image/')) {
            const logData = {
                url: '/images/post',
                method: 'POST',
                timestamp: new Date(),
                success: false,
                descripcion: 'No se subió ninguna imagen o el archivo no es una imagen válida',
            };

            await connection.promise().query('INSERT INTO request_logs SET ?', logData);

            return res.status(400).send('No se subió ninguna imagen o el archivo no es una imagen válida');
        }

        const type = req.file.mimetype;
        const name = req.file.originalname;
        const data = fs.readFileSync(path.join(__dirname, '../images/' + req.file.filename));

        await connection.promise().query('INSERT INTO image SET ?', [{ type, nombre: name, data }]);

        console.log('Imagen guardada exitosamente');

        const logData = {
            url: '/images/post',
            method: 'POST',
            timestamp: new Date(),
            contentType: type,
            descripcion: 'Inserción de imagen',
            success: true
        };

        await connection.promise().query('INSERT INTO request_logs SET ?', logData);

        res.send('Imagen guardada exitosamente');
    } catch (error) {
        console.error('Error al procesar la solicitud:', error);

        const logData = {
            url: '/images/post',
            method: 'POST',
            timestamp: new Date(),
            success: false,
            errorMessage: error.message
        };

        await connection.promise().query('INSERT INTO request_logs SET ?', logData);

        res.status(500).send('Error al procesar la solicitud');
    }
});

  router.get('/images/get', (req, res) => {
  
      connection.query('Select * from image', (err, rows) => {
        if (err) {_
          console.error('Error al mostrar: ', err)
          return res.status(500).send('Error de servidor');
        }
        //Iteramos la información de consulta en un array
        rows.map(img =>{ 
            fs.writeFileSync(path.join(__dirname, '../db_images/'+ img.id + '-henka.png'), img.data)
        })
        const images_dir = fs.readdirSync(path.join(__dirname, '../db_images/'))
        res.json(images_dir)
      });
    });
  
    router.delete('/images/delete/:id', async (req, res) => {
      try {
          const imagePath = path.join(__dirname, '../db_images/' + req.params.id + '-henka.png');
          if (!fs.existsSync(imagePath)) {
              const logData = {
                  url: '/images/delete/' + req.params.id,
                  method: 'DELETE',
                  timestamp: new Date(),
                  success: false,
                  descripcion: 'No se encontró la imagen a borrar'
              };
  
              await connection.promise().query('INSERT INTO request_logs SET ?', logData);
  
              return res.status(404).send('No se encontró la imagen a borrar');
          }
  
          await connection.promise().query('DELETE FROM image WHERE id = ?', [req.params.id]);
          fs.unlinkSync(imagePath);
  
          const logData = {
              url: '/images/delete/' + req.params.id,
              method: 'DELETE',
              timestamp: new Date(),
              success: true,
              descripcion: 'Imagen borrada con éxito'
          };
  
          await connection.promise().query('INSERT INTO request_logs SET ?', logData);
  
          res.json('Imagen borrada');
      } catch (error) {
          console.error('Error al procesar la solicitud:', error);
  
          const logData = {
              url: '/images/delete/' + req.params.id,
              method: 'DELETE',
              timestamp: new Date(),
              success: false,
              errorMessage: error.message
          };
  
          await connection.promise().query('INSERT INTO request_logs SET ?', logData);
  
          res.status(500).send('Error al procesar la solicitud');
      }
  });
  
  router.post('/login', (req, res) =>{
    const { username, password } = req.body;

    connection.query('SELECT * FROM usuario WHERE usuario = ?', [username], async (err, rows) => {
        if (err) {
            const logData = {
                url: '/login',
                method: 'POST',
                timestamp: new Date(),
                success: false,
                descripcion: 'Error de servidor'
            };

            await connection.promise().query('INSERT INTO request_logs SET ?', logData);

            return res.status(500).send('Error de servidor');
        }
        if (rows.length > 0) {
            const hashedPassword = rows[0].contraseña;

            bcrypt.compare(password, hashedPassword, async (err, result) => {
                if (err) {
                    const logData = {
                        url: '/login',
                        method: 'POST',
                        timestamp: new Date(),
                        success: false,
                        descripcion: 'Error de servidor'
                    };

                    await connection.promise().query('INSERT INTO request_logs SET ?', logData);

                    return res.status(500).send('Error de servidor');
                }
                if (result) {
                    const token = jwt.sign({ userId: rows[0].id }, 'secret-key-token', { expiresIn: '2hrs' });
                    const logData = {
                        url: '/login',
                        method: 'POST',
                        timestamp: new Date(),
                        descripcion:'Inicio de sesion exitoso',
                        success: true
                    };

                    await connection.promise().query('INSERT INTO request_logs SET ?', logData);

                    return res.json({ token });
                } else {
                    const logData = {
                        url: '/login',
                        method: 'POST',
                        timestamp: new Date(),
                        descripcion: 'Intento de login fue denegado',
                        success: false,
                    };

                    await connection.promise().query('INSERT INTO request_logs SET ?', logData);

                    return res.json({ error: 'Contraseña incorrecta' });
                }
            });
        } else {
            const logData = {
                url: '/login',
                method: 'POST',
                timestamp: new Date(),
                descripcion: 'No existe el usuario',
                success: false
            };

            await connection.promise().query('INSERT INTO request_logs SET ?', logData);

            return res.json({ error: 'No existe el usuario' });
        }
    });
});
  
  router.post('/register', async (req, res) => {
    try {
      const { username, password } = req.body;
  
      connection.query('SELECT * FROM usuario WHERE usuario = ?', [username], (err, rows) => {
        if (err) {
          return res.status(500).send('Error de servidor');
        }
  
        if (rows.length > 0) {
          return res.status(400).json({ error: 'El usuario ya existe' });
        }
  
        bcrypt.hash(password, 10, (err, hashedPassword) => {
          if (err) {
            return res.status(500).send('Error de servidor');
          }
  
          connection.query('INSERT INTO usuario (usuario, contraseña) VALUES (?, ?)', [username, hashedPassword], (err) => {
            if (err) {
              return res.status(500).send('Error de servidor');
            }
  
            return res.status(201).json({ message: 'Usuario creado correctamente' });
          });
        });
      });
    } catch (error) {
      console.error('Error al registrar usuario:', error);
      return res.status(500).send('Error de servidor');
    }
  });

    router.get('/logs', (req, res) =>{
      connection.query('select * from request_logs', (err, result)=>{
        if (err) return res.json({Message:'Error de servidor'});
        return res.json(result);
      })
    })

  router.get('/concepto0', (req, res) =>{
    connection.query('select * from descripcion', (err, result)=>{
      if (err) return res.json({Message:'Error de servidor'});
      return res.json(result);
    })
  })

  router.get('/concepto1', (req, res) =>{
    connection.query('select * from ventajasP', (err, result)=>{
      if (err) return res.json({Message:'Error de servidor'});
      return res.json(result);
    })
  })

  router.get('/concepto2', (req, res) =>{
    connection.query('select * from ventajasPai', (err, result)=>{
      if (err) return res.json({Message:'Error de servidor'});
      return res.json(result);
    })
  })

  router.get('/concepto3', (req, res) =>{
    connection.query('select * from ventajasAuto', (err, result)=>{
      if (err) return res.json({Message:'Error de servidor'});
      return res.json(result);
    })
  })

  router.get('/info', (req, res) =>{
    connection.query('select * from contacto', (err, result)=>{
      if (err) return res.json({Message:'Error de servidor'});
      return res.json(result);
    })
  })

  router.get('/horario', (req, res) =>{
    connection.query('select * from horario', (err, result)=>{
      if (err) return res.json({Message:'Error de servidor'});
      return res.json(result);
    })
  })

  router.get('/mision', (req, res) =>{
    connection.query('select * from misvis', (err, result)=>{
      if (err) return res.json({Message:'Error de servidor'});
      return res.json(result);
    })
  })

  router.get('/valores', (req, res) =>{
    connection.query('select * from valores', (err, result)=>{
      if (err) return res.json({Message:'Error de servidor'});
      return res.json(result);
    })
  })

  router.get('/aplicacion-Pin', (req, res) =>{
    connection.query('select * from ApliPin', (err, result)=>{
      if (err) return res.json({Message:'Error de servidor'});
      return res.json(result);
    })
  })

  router.get('/aplicacion-Pai', (req, res) =>{
    connection.query('select * from ApliPai', (err, result)=>{
      if (err) return res.json({Message:'Error de servidor'});
      return res.json(result);
    })
  })

  router.get('/aplicacion-Auto', (req, res) =>{
    connection.query('select * from ApliAuto', (err, result)=>{
      if (err) return res.json({Message:'Error de servidor'});
      return res.json(result);
    })
  })

  router.post('/gallery/post', fileUploadImagenes, async (req, res) => {
    try {
        if (!req.file || !req.file.mimetype.startsWith('image/')) {
            const logData = {
                url: '/gallery/post',
                method: 'POST',
                timestamp: new Date(),
                success: false,
                descripcion: 'No se subió ninguna imagen o el archivo no es una imagen válida'
            };

            await connection.promise().query('INSERT INTO request_logs SET ?', logData);

            return res.status(400).send('No se subió ninguna imagen o el archivo no es una imagen válida');
        }

        const color = req.body.color;
        const type = req.file.mimetype;
        const name = req.file.originalname;
        const data = fs.readFileSync(path.join(__dirname, '../imagenes/', req.file.filename));

        const image = { color: color, type: type, nombre: name, data: data };

        await connection.promise().query('INSERT INTO gallery SET ?', image);

        const logData = {
            url: '/gallery/post',
            method: 'POST',
            timestamp: new Date(),
            success: true,
            descripcion: 'Imagen guardada en la galería'
        };

        await connection.promise().query('INSERT INTO request_logs SET ?', logData);

        res.send('Imagen guardada');
    } catch (error) {
        console.error('Error al procesar la solicitud:', error);

        const logData = {
            url: '/gallery/post',
            method: 'POST',
            timestamp: new Date(),
            success: false,
            errorMessage: error.message
        };

        await connection.promise().query('INSERT INTO request_logs SET ?', logData);

        res.status(500).send('Error al procesar la solicitud');
    }
});

    router.get('/gallery/get', (req, res) => {
  
      connection.query('Select * from gallery', (err, rows) => {
        if (err) {
          console.error('Error al mostrar: ', err);
          return res.status(500).send('Error de servidor');
        }
        //Iteramos la información de consulta en un array
        rows.map(img =>{ 
            fs.writeFileSync(path.join(__dirname, '../db_imagenes/'+ img.id + '-h_sukiru.png'), img.data)
        })
        const images_dir = fs.readdirSync(path.join(__dirname, '../db_imagenes/'))
        res.json(images_dir)
      });
    });

    router.get('/gallery/color', (req, res) => {
      const color = req.query.color;
      let query = 'SELECT * FROM gallery';
    
      if (color) {
        query += ' WHERE color = ?';
      }
    
      connection.query(query, [color], (err, rows) => {
        if (err) {
          console.error('Error al mostrar: ', err);
          return res.status(500).send('Error de servidor');
        }
    
        const images = [];
    
        for (const row of rows) {
          const imagePath = path.join(__dirname, '../db_imagenes', `${row.id}-h_sukiru.png`);
          images.push(imagePath);
        }
    
        res.json(images);
      });
    });
    
    router.delete('/gallery/delete/:id', async (req, res) => {
      try {
          const image = await connection.promise().query('SELECT * FROM gallery WHERE id = ?', [req.params.id]);
  
          if (image.length === 0) {
              const logData = {
                  url: '/gallery/delete/' + req.params.id,
                  method: 'DELETE',
                  timestamp: new Date(),
                  success: false,
                  descripcion: 'No se encontró ninguna imagen en la galería con el ID proporcionado'
              };
  
              await connection.promise().query('INSERT INTO request_logs SET ?', logData);
  
              return res.status(404).send('No se encontró ninguna imagen en la galería con el ID proporcionado');
          }
  
          fs.unlinkSync(path.join(__dirname, '../db_imagenes/' + req.params.id + '-h_sukiru.png'));
          await connection.promise().query('DELETE FROM gallery WHERE id = ?', [req.params.id]);
  
          const logData = {
              url: '/gallery/delete/' + req.params.id,
              method: 'DELETE',
              timestamp: new Date(),
              success: true,
              descripcion: 'Imagen borrada exitosamente de la galería por color'
          };
  
          await connection.promise().query('INSERT INTO request_logs SET ?', logData);
  
          res.json('Imagen borrada exitosamente de la galería');
      } catch (error) {
          console.error('Error al procesar la solicitud:', error);
  
          const logData = {
              url: '/gallery/delete/' + req.params.id,
              method: 'DELETE',
              timestamp: new Date(),
              success: false,
              errorMessage: error.message
          };
  
          await connection.promise().query('INSERT INTO request_logs SET ?', logData);
  
          res.status(500).send('Error al procesar la solicitud');
      }
  });
  
  router.put('/update', (req, res) => {
    const { ubicacion, numero, email, id, descripcion } = req.body;
    
    if (!descripcion) {
        const logData = {
            url: '/update',
            method: 'PUT',
            timestamp: new Date(),
            success: false,
            descripcion: 'La descripción no fue proporcionada'
        };

        connection.query('INSERT INTO request_logs SET ?', logData, (err, result) => {
            if (err) {
                console.error('Error al insertar el log:', err);
            }
        });

        return res.status(400).send('La descripción no fue proporcionada');
    }

    connection.query('UPDATE contacto SET ubicacion = ?, numero = ?, email = ?, descripcion = ? WHERE id = ?', [ubicacion, numero, email, descripcion, id], (err, rows) => {
        if (err) {
            console.error('Error al actualizar la consulta: ', err);

            const logData = {
                url: '/update',
                method: 'PUT',
                timestamp: new Date(),
                success: false,
                errorMessage: err.message
            };

            connection.query('INSERT INTO request_logs SET ?', logData, (logErr, logResult) => {
                if (logErr) {
                    console.error('Error al insertar el log:', logErr);
                }
            });

            return res.status(500).send('Error de servidor');
        }

        console.log('Consulta actualizada');

        const logData = {
            url: '/update',
            method: 'PUT',
            timestamp: new Date(),
            success: true,
            descripcion: 'Información actualizada de Contacto'
        };

        connection.query('INSERT INTO request_logs SET ?', logData, (logErr, logResult) => {
            if (logErr) {
                console.error('Error al insertar el log:', logErr);
            }
        });

        res.send('Consulta actualizada exitosamente');
    });
});

router.put('/update0', (req, res) => {
  const { descripcion, id } = req.body;
  if (!descripcion) {
      const logData = {
          url: '/update0',
          method: 'PUT',
          timestamp: new Date(),
          success: false,
          descripcion: 'La descripción no fue proporcionada'
      };

      connection.query('INSERT INTO request_logs SET ?', logData, (err, result) => {
          if (err) {
              console.error('Error al insertar el log:', err);
          }
      });

      return res.status(400).send('La descripción no fue proporcionada');
  }

  connection.query('UPDATE descripcion SET descripcion = ? WHERE id = ?', [descripcion, id], (err, rows) => {
      if (err) {
          console.error('Error al actualizar la consulta: ', err);

          const logData = {
              url: '/update0',
              method: 'PUT',
              timestamp: new Date(),
              success: false,
              errorMessage: err.message
          };

          connection.query('INSERT INTO request_logs SET ?', logData, (logErr, logResult) => {
              if (logErr) {
                  console.error('Error al insertar el log:', logErr);
              }
          });

          return res.status(500).send('Error de servidor');
      }

      console.log('Consulta actualizada');

      const logData = {
          url: '/update0',
          method: 'PUT',
          timestamp: new Date(),
          success: true,
          descripcion: 'Información actualizada de descripción'
      };

      connection.query('INSERT INTO request_logs SET ?', logData, (logErr, logResult) => {
          if (logErr) {
              console.error('Error al insertar el log:', logErr);
          }
      });

      res.json('Consulta actualizada exitosamente');
  });
});

router.put('/update1', (req, res) => {
  const { titulo, descripcion, id } = req.body;
  if (!titulo || !descripcion || !id) {
      return res.status(400).send('Falta título, descripción o ID');
  }

  connection.query('UPDATE ventajasP SET titulo = ?, descripcion = ? WHERE id = ?', [titulo, descripcion, id], (err, rows) => {
      if (err) {
          console.error('Error al actualizar la consulta: ', err);
          return res.status(500).send('Error de servidor');
      }

      console.log('Consulta actualizada');

      const logData = {
          url: '/update1',
          method: 'PUT',
          timestamp: new Date(),
          success: true,
          descripcion: 'Información actualizada de ventajasP'
      };

      connection.query('INSERT INTO request_logs SET ?', logData, (logErr, logResult) => {
          if (logErr) {
              console.error('Error al insertar el log:', logErr);
          }
      });

      res.json('Consulta actualizada exitosamente');
  });
});

router.put('/update2', (req, res) => {
  const { titulo, descripcion, id } = req.body;
  if (!titulo || !descripcion || !id) {
      return res.status(400).send('Falta título, descripción o ID');
  }

  connection.query('UPDATE ventajasPai SET titulo = ?, descripcion = ? WHERE id = ?', [titulo, descripcion, id], (err, rows) => {
      if (err) {
          console.error('Error al actualizar la consulta: ', err);
          return res.status(500).send('Error de servidor');
      }

      console.log('Consulta actualizada');

      const logData = {
          url: '/update2',
          method: 'PUT',
          timestamp: new Date(),
          success: true,
          descripcion: 'Información actualizada de ventajas Pailería'
      };

      connection.query('INSERT INTO request_logs SET ?', logData, (logErr, logResult) => {
          if (logErr) {
              console.error('Error al insertar el log:', logErr);
          }
      });

      res.json('Consulta actualizada exitosamente');
  });
});

router.put('/update3', (req, res) => {
  const { titulo, descripcion, id } = req.body;
  if (!titulo || !descripcion || !id) {
      return res.status(400).send('Falta título, descripción o ID');
  }

  connection.query('UPDATE ventajasAuto SET titulo = ?, descripcion = ? WHERE id = ?', [titulo, descripcion, id], (err, rows) => {
      if (err) {
          console.error('Error al actualizar la consulta: ', err);
          return res.status(500).send('Error de servidor');
      }

      console.log('Consulta actualizada');

      const logData = {
          url: '/update3',
          method: 'PUT',
          timestamp: new Date(),
          success: true,
          descripcion: 'Información actualizada de ventajas Automatizacion'
      };

      connection.query('INSERT INTO request_logs SET ?', logData, (logErr, logResult) => {
          if (logErr) {
              console.error('Error al insertar el log:', logErr);
          }
      });

      res.json('Consulta actualizada exitosamente');
  });
});

router.put('/update4', (req, res) => {
  const { mision, id } = req.body;
  if (!mision || !id) {
      return res.status(400).send('Falta misión o ID');
  }

  connection.query('UPDATE misvis SET mision = ? WHERE id = ?', [mision, id], (err, rows) => {
      if (err) {
          console.error('Error al actualizar la consulta: ', err);
          return res.status(500).send('Error de servidor');
      }

      console.log('Consulta actualizada');

      const logData = {
          url: '/update4',
          method: 'PUT',
          timestamp: new Date(),
          success: true,
          descripcion: 'Información actualizada de Mision y Vision'
      };

      connection.query('INSERT INTO request_logs SET ?', logData, (logErr, logResult) => {
          if (logErr) {
              console.error('Error al insertar el log:', logErr);
          }
      });

      res.json('Consulta actualizada exitosamente');
  });
});

router.put('/update5', (req, res) => {
  const { horario, id } = req.body;
  if (!horario || !id) {
      return res.status(400).send('Falta horario o ID');
  }

  connection.query('UPDATE horario SET horario = ? WHERE id = ?', [horario, id], (err, rows) => {
      if (err) {
          console.error('Error al actualizar la consulta: ', err);
          return res.status(500).send('Error de servidor');
      }

      console.log('Consulta actualizada');

      const logData = {
          url: '/update5',
          method: 'PUT',
          timestamp: new Date(),
          success: true,
          descripcion: 'Horarios actualizado'
      };

      connection.query('INSERT INTO request_logs SET ?', logData, (logErr, logResult) => {
          if (logErr) {
              console.error('Error al insertar el log:', logErr);
          }
      });

      res.json('Consulta actualizada exitosamente');
  });
});

router.put('/update6', (req, res) => {
  const { valor, descripcion, id } = req.body;
  if (!valor || !descripcion || !id) {
      return res.status(400).send('Falta valor, descripción o ID');
  }

  connection.query('UPDATE valores SET valor = ?, descripcion = ? WHERE id = ?', [valor, descripcion, id], (err, rows) => {
      if (err) {
          console.error('Error al actualizar la consulta: ', err);
          return res.status(500).send('Error de servidor');
      }

      console.log('Consulta actualizada');

      const logData = {
          url: '/update6',
          method: 'PUT',
          timestamp: new Date(),
          success: true,
          descripcion: 'Información de valores actualizada'
      };

      connection.query('INSERT INTO request_logs SET ?', logData, (logErr, logResult) => {
          if (logErr) {
              console.error('Error al insertar el log:', logErr);
          }
      });

      res.json('Consulta actualizada exitosamente');
  });
});

router.put('/update7', (req, res) => {
  const { apli, descripcion, id } = req.body;
  if (!apli || !descripcion || !id) {
      return res.status(400).send('Falta aplicación, descripción o ID');
  }

  connection.query('UPDATE ApliPin SET aplicacion = ?, descripcion = ? WHERE id = ?', [apli, descripcion, id], (err, rows) => {
      if (err) {
          console.error('Error al actualizar la consulta: ', err);
          return res.status(500).send('Error de servidor');
      }

      console.log('Consulta actualizada');

      const logData = {
          url: '/update7',
          method: 'PUT',
          timestamp: new Date(),
          success: true,
          descripcion: 'Información de las Aplicaciones la Pintura actualizada'
      };

      connection.query('INSERT INTO request_logs SET ?', logData, (logErr, logResult) => {
          if (logErr) {
              console.error('Error al insertar el log:', logErr);
          }
      });

      res.json('Consulta actualizada exitosamente');
  });
});

router.put('/update8', (req, res) => {
  const { apli, descripcion, id } = req.body;
  if (!apli || !descripcion || !id) {
      return res.status(400).send('Falta aplicación, descripción o ID');
  }

  connection.query('UPDATE ApliPai SET aplicacion = ?, descripcion = ? WHERE id = ?', [apli, descripcion, id], (err, rows) => {
      if (err) {
          console.error('Error al actualizar la consulta: ', err);
          return res.status(500).send('Error de servidor');
      }

      console.log('Consulta actualizada');

      const logData = {
          url: '/update8',
          method: 'PUT',
          timestamp: new Date(),
          success: true,
          descripcion: 'Información de ApliPai actualizada'
      };

      connection.query('INSERT INTO request_logs SET ?', logData, (logErr, logResult) => {
          if (logErr) {
              console.error('Error al insertar el log:', logErr);
          }
      });

      res.json('Consulta actualizada exitosamente');
  });
});

router.put('/update9', (req, res) => {
  const { apli, descripcion, id } = req.body;
  if (!apli || !descripcion || !id) {
      return res.status(400).send('Falta aplicación, descripción o ID');
  }

  connection.query('UPDATE ApliAuto SET aplicacion = ?, descripcion = ? WHERE id = ?', [apli, descripcion, id], (err, rows) => {
      if (err) {
          console.error('Error al actualizar la consulta: ', err);
          return res.status(500).send('Error de servidor');
      }

      console.log('Consulta actualizada');

      const logData = {
          url: '/update9',
          method: 'PUT',
          timestamp: new Date(),
          success: true,
          descripcion: 'Información de ApliAuto actualizada'
      };

      connection.query('INSERT INTO request_logs SET ?', logData, (logErr, logResult) => {
          if (logErr) {
              console.error('Error al insertar el log:', logErr);
          }
      });

      res.json('Consulta actualizada exitosamente');
  });
});

  router.delete('/delete0/:id', (req, res) => {
    connection.query('Delete from descripcion where id = ?', [req.params.id], (err, rows) => {
      if (err) {
        console.error('Error al mostrar: ', err);
        return res.status(500).send('Error de servidor');
      }
      else {
        res.json('Descripción borrada')
        console.log('Se borró con exito')
      }
    });
  });

  router.delete('/usuario/:id', (req, res) => {
    connection.query('Delete from usuario where id = ?', [req.params.id], (err, rows) => {
      if (err) {
        console.error('Error al mostrar: ', err);
        return res.status(500).send('Error de servidor');
      }
      else {
        res.json('Descripción borrada')
        console.log('Se borró con exito')
      }
    });
  });

});


module.exports = router;